COMPILER=clang++
FLAGS=-O3 -std=c++11 -Werror -Wall -Wextra -W -I.
FILES=ROBDD/ROBDDFactory.cc ROBDD/ROBDD.cc ROBDD/Operations.cc Demo/8queens.cc

all:
	$(COMPILER) $(FLAGS) $(FILES)
