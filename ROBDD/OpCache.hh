#ifndef ROBDD_OPCASH
# define ROBDD_OPCASH

# include <ROBDD/ROBDD.hh>

namespace std
{
  template<>
    class hash<std::pair<robdd::ROBDD*, robdd::ROBDD*>>
    {
      public:
        size_t operator()(const pair<robdd::ROBDD*, robdd::ROBDD*>& s) const 
        {
          size_t h1 = hash<robdd::ROBDD*>()(get<0>(s));
          size_t h2 = hash<robdd::ROBDD*>()(get<1>(s));
          return h1 ^ (h2 << 1); // FIXME: dont know if this is a good hache function…
        }
    };
}; // end std

namespace robdd
{
  template <typename op>
    class OpCache
    {
      private:
        typedef std::unordered_map<std::pair<ROBDD*, ROBDD*>, ROBDD*> OpCacheType;
        static OpCacheType _opCache;

      public:
        static ROBDD* getOp(ROBDD* left, ROBDD* right)
        {
          auto cacheedOp = _opCache.find(std::make_pair(left, right));

          if (cacheedOp == _opCache.end())
            return nullptr;

          return std::get<1>(*cacheedOp);
        }

        static ROBDD* registerOp(ROBDD* left, ROBDD* right, ROBDD* result)
        {
          _opCache[std::make_pair(left, right)] = result;

          return result;
        }
    };

  template <typename op>
    typename OpCache<op>::OpCacheType OpCache<op>::_opCache = OpCache::OpCacheType();
} // end robdd

#endif
