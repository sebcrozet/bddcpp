#ifndef ROBDD_ROBDD
# define ROBDD_ROBDD

namespace robdd
{
  class ROBDD
  {
    private:
      ROBDD* _left;
      ROBDD* _right;
      int    _var;
    public:
      static int compare(const ROBDD* a, const ROBDD* b)
      { return a->_var - b->_var; }

      ROBDD(ROBDD* left, int var, ROBDD* right);

      bool isLeaf() const
      { return _left == _right; }

      int  var() const
      { return _var; }

      ROBDD* left() const
      { return _left; }

      ROBDD* right() const
      { return _right; }
  };
} // end robdd

#endif // end ROBDD_ROBDD
