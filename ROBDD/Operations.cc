#include <ROBDD/Operations.hh>

namespace robdd
{
  bool getSat(const ROBDD* bdd, std::vector<std::pair<bool, const ROBDD*>>& outsat)
  {
    if (bdd == ROBDDFactory::one())
      return true;

    if (bdd == ROBDDFactory::zero())
      return false;

    if (getSat(bdd->left(), outsat))
    {
      outsat.push_back(std::make_pair(false, bdd));
      return true;
    }

    if (getSat(bdd->right(), outsat))
    {
      outsat.push_back(std::make_pair(true, bdd));
      return true;
    }

    return false;
  }
} // end robdd
