#include <ROBDD/ROBDD.hh>

namespace robdd
{
  ROBDD::ROBDD(ROBDD* left, int var, ROBDD* right)
    : _left(left), _right(right), _var(var)
  { }
} // end ROBDD
