#ifndef ROBDD_ROBDDFACTORY
# define ROBDD_ROBDDFACTORY

# include <tuple>
# include <unordered_map>

# include <ROBDD/ROBDD.hh>

namespace robdd
{
  class ROBDDFactory
  {
    private:
      typedef std::unordered_map<std::tuple<ROBDD*, int, ROBDD*>, ROBDD*> BDDCash;
      static BDDCash _bddCash;
      static ROBDD*  _one;
      static ROBDD*  _zero;

    public:
      static ROBDD* mkNode(ROBDD* left, int var, ROBDD* right);
      static ROBDD* singleton(int var);
      static ROBDD* singletonNot(int var);

      // FIXME: const-ify that?
      static ROBDD* one();
      static ROBDD* zero();
  };
} // end robdd

namespace std
{
  template<>
    class hash<std::tuple<robdd::ROBDD*, int, robdd::ROBDD*>>
    {
      public:
        size_t operator()(const tuple<robdd::ROBDD*, int, robdd::ROBDD*>& s) const 
        {
          size_t h1 = hash<robdd::ROBDD*>()(get<0>(s));
          size_t h2 = hash<int>()(get<1>(s));
          size_t h3 = hash<robdd::ROBDD*>()(get<2>(s));
          return h1 ^ (h2 << 1) ^ (h3 << 1); // FIXME: dont know if this is a good hash function…
        }
    };
}; // end std


#endif // ROBDD_ROBDDFACTORY
