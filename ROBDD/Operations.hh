#ifndef ROBDD_OPERATIONS
# define ROBDD_OPERATIONS

# include <cassert>
# include <iostream>
# include <vector>
# include <ROBDD/ROBDD.hh>
# include <ROBDD/ROBDDFactory.hh>
# include <ROBDD/OpCache.hh>

namespace robdd
{
  bool getSat(const ROBDD* bdd, std::vector<std::pair<bool, const ROBDD*>>& outsat);

  template <typename op>
    ROBDD* unary(ROBDD* a)
    {
      ROBDD* cashedOperation = OpCache<op>::getOp(a, a);

      if (cashedOperation)
        return cashedOperation;

      if (!a->isLeaf())
        return OpCache<op>::registerOp(
                 a, a,
                 ROBDDFactory::mkNode(unary<op>(a->left()), a->var(), unary<op>(a->right()))
                 );

      if (op::unaryOp(a->var()))
        return ROBDDFactory::one();

      return ROBDDFactory::zero();
    }

  template <typename op>
    ROBDD* binary(ROBDD* a, ROBDD* b)
    {
      ROBDD* cashedOperation = OpCache<op>::getOp(a, b);

      if (cashedOperation)
        return cashedOperation;

      bool aleaf = a->isLeaf();
      bool bleaf = b->isLeaf();

      if (!aleaf)
      {
        if (bleaf)
        {
          return OpCache<op>::registerOp(a, b, ROBDDFactory::mkNode(binary<op>(a->left(), b)
                                              , a->var()
                                              , binary<op>(a->right(), b)));
        }

        // nobody is a leaf
        int cmp = ROBDD::compare(a, b);

        if (cmp == 0)
        {
          return OpCache<op>::registerOp(a, b, ROBDDFactory::mkNode(binary<op>(a->left(), b->left())
                                              , a->var()
                                              , binary<op>(a->right(), b->right())));
        }
        if (cmp < 0)
        {
          return OpCache<op>::registerOp(a, b, ROBDDFactory::mkNode(binary<op>(a->left(), b)
                                              , a->var()
                                              , binary<op>(a->right(), b)));
        }

        // cmp > 0
        return OpCache<op>::registerOp(a, b, ROBDDFactory::mkNode(binary<op>(a, b->left())
                                            , b->var()
                                            , binary<op>(a, b->right())));
      }

      if (!bleaf)
      { // a is a leaf
        return OpCache<op>::registerOp(a, b, ROBDDFactory::mkNode(binary<op>(a, b->left())
                                            , b->var()
                                            , binary<op>(a, b->right())));
      }

      // two leaves, apply the binary op
      assert(a->var() == 1 || a->var() == 0);
      assert(b->var() == 1 || b->var() == 0);

      if (op::op(a->var(), b->var()))
        return ROBDDFactory::one();

      return ROBDDFactory::zero();
    }

  struct And
  {
    static bool op(int a, int b)
    { return a && b; }
  };

  struct Or
  {
    static bool op(int a, int b)
    { return a || b; }
  };

  struct Imply
  {
    static bool op(int a, int b)
    { return !(bool)a || b; }
  };

  struct Equiv
  {
    static bool op(int a, int b)
    { return (bool)a == (bool)b; }
  };

  struct Not
  {
    static bool unaryOp(int a)
    { return !(bool)a; }
  };

  inline
  ROBDD* operator&&(ROBDD& a, ROBDD& b)
  {
    return binary<And>(&a, &b);
  }

  inline
  ROBDD* operator||(ROBDD& a, ROBDD& b)
  {
    return binary<Or>(&a, &b);
  }

  inline
  ROBDD* operator>>(ROBDD& a, ROBDD& b)
  {
    return binary<Imply>(&a, &b);
  }

  inline
  ROBDD* negate(ROBDD& a)
  {
    return unary<Not>(&a);
  }
} // end robdd

#endif // ROBDD_OPERATIONS
