#include <iostream>
#include <cassert>
#include <tuple>
#include <ROBDD/ROBDDFactory.hh>

namespace robdd
{

  ROBDD* ROBDDFactory::_one  = new ROBDD(nullptr, 1, nullptr);
  ROBDD* ROBDDFactory::_zero = new ROBDD(nullptr, 0, nullptr);
  ROBDDFactory::BDDCash ROBDDFactory::_bddCash = BDDCash();

  ROBDD* ROBDDFactory::one()
  { return _one; }

  ROBDD* ROBDDFactory::zero()
  { return _zero; }

  ROBDD* ROBDDFactory::singleton(int var)
  {
    return mkNode(_zero, var, _one);
  }

  ROBDD* ROBDDFactory::singletonNot(int var)
  {
    return mkNode(_one, var, _zero);
  }

  ROBDD* ROBDDFactory::mkNode(ROBDD* left, int var, ROBDD* right)
  {
    assert(var != 1 && var != 0);
    assert((left == _zero || left == _one || left->var() > var)
           && (right == _zero || right == _one || right->var() > var));

    if (left == right)
      return left;

    auto key       = std::make_tuple(left, var, right);
    auto cashedBdd = _bddCash.find(key);

    if (cashedBdd != _bddCash.end())
      return std::get<1>(*cashedBdd);

    auto res = new ROBDD(left, var, right);

    _bddCash[key] = res;

    return res;
  }
} // end robdd
