#include <iostream>
#include <ROBDD/ROBDD.hh>
#include <ROBDD/Operations.hh>

using namespace robdd;

int main(int, char** argv)
{
  int n = atoi(argv[1]);

  std::cout << n << " queens" << std::endl;

  ROBDD* variables[n][n];
  ROBDD* notVariables[n][n];
  ROBDD* queens = ROBDDFactory::one();

  for (int i = 0; i < n; ++i)
  {
    for (int j = 0; j < n; ++j)
    {
      variables[i][j]    = ROBDDFactory::singleton(2 + i * n + j);
      notVariables[i][j] = ROBDDFactory::singletonNot(2 + i * n + j);
    }
  }

  // one queen per line…
  for (int i = 0; i < n; ++i)
  {
    ROBDD* linecond = ROBDDFactory::zero();

    for (int j = 0; j < n; ++j)
      linecond = *linecond || *variables[i][j];

    queens = *queens && *linecond;
  }

  // …and we forbid quite a lot of axis-alligned/diagonal cells
  for (int i = 0; i < n; ++i)
  {
    for (int j = 0; j < n; ++j)
    {
      ROBDD* forbidden = ROBDDFactory::one();

      // right
      for (int k = j + 1; k < n; ++k)
        forbidden = *forbidden && *notVariables[i][k];
      // left
      for (int k = j - 1; k >= 0; --k)
        forbidden = *forbidden && *notVariables[i][k];
      // up
      for (int k = i - 1; k >= 0; --k)
        forbidden = *forbidden && *notVariables[k][j];
      // down
      for (int k = i + 1; k < n; ++k)
        forbidden = *forbidden && *notVariables[k][j];
      // dr
      for (int k = i + 1, l = j + 1; k < n && l < n; ++k, ++l)
          forbidden = *forbidden && *notVariables[k][l];
      // ur
      for (int k = i - 1, l = j + 1; k >= 0 && l < n; --k, ++l)
          forbidden = *forbidden && *notVariables[k][l];
      // ul
      for (int k = i - 1, l = j - 1; k >= 0 && l >= 0; --k, --l)
          forbidden = *forbidden && *notVariables[k][l];
      // dl
      for (int k = i + 1, l = j - 1; k < n && l >= 0; ++k, --l)
          forbidden = *forbidden && *notVariables[k][l];

      queens = *queens && *(*variables[i][j] >> *forbidden);
    }
  }

  std::vector<std::pair<bool, const ROBDD*>> sat;
  std::cout << getSat(queens, sat) << std::endl;
  for (auto res : sat)
  {
    auto var = std::get<1>(res)->var();

    if (std::get<0>(res))
      std::cout << "(" << (var - 2) / n << "," << (var - 2) % n << ") ";
  }
  std::cout << std::endl;
}
